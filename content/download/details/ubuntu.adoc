+++
title = "Install on Ubuntu"
summary = "Install instructions for KiCad on Ubuntu"
+++

== {{< param "release" >}} Stable Release

=== Installation
KiCad {{< param "release" >}} is available in https://launchpad.net/~kicad/+archive/ubuntu/kicad-7.0-releases[PPA for KiCad: 7.0 releases ].
include::./content/download/_kicad_ppa.adoc[]


== 6.0 Old Stable Release

KiCad 6.0 is available in https://launchpad.net/~kicad/+archive/ubuntu/kicad-6.0-releases[PPA for KiCad: 6.0 releases].