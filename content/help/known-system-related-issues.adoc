+++
title = "Known System Related Issues"
summary = "Known system related issues with KiCad on various platforms"
aliases = [ "/post/known-system-related-issues/" ]
[menu.main]
    parent = "Help"
    name   = "Known System Related Issues"
  weight = 50
+++
:toc: macro
:toc-title:

toc::[]

This page contains a number of known KiCad issues that are specific to a certain platform or
hardware configuration.

== All Platforms

=== High DPI Displays

Recent versions of KiCad (7.x and newer) should work well on high DPI displays (systems with the
display scaling factor set to something other than 100%).  Please report any bugs or incorrect
rendering when using a high DPI display.

Older versions of KiCad, or those compiled against older verisons of wxWidgets, are known to have
many issues with high DPI displays.

== Linux Specific Issues

=== Radeon Graphics

Some users with Radeon graphics have been experiencing problems with
slow rendering in the Gerber viewer and schematic editor.
https://bugs.launchpad.net/kicad/+bug/1003859[Some]
https://bugs.launchpad.net/kicad/+bug/1186813[bugs] have been opened related to Radeon cards.
The solution seems to be require changing an Xorg setting which is either
`Option "EXAPixmaps" "off"` or `Option "ShadowPrimary"` (see the bug reports for details).

=== Wayland

It is known that KiCad does not work well under Wayland. There are a number
of known issues with wxWidgets and Wayland.
link:https://github.com/wxWidgets/wxWidgets/labels/Wayland[See the wxWidgets bug tracker for details].

KiCad requests the XWayland compatibility layer when starting, however this is
an emulation mode and issues arising while using this mode need to be recreated
under X11 before they will be addressed.

At the moment, Wayland has no support for cursor warping, which means that the KiCad features of
centering the cursor when zooming, and continously panning the canvas, are broken on Wayland
systems.

=== MATE

Some versions of MATE (on Ubuntu or Linux Mint) together with GTK3 and wxWidgets can lead to
rendering problems and keyboard keys that do not work or can freeze the GUI input. They are
potentially caused by the ibus. To solve it, install the packages ibus-gtk and ibus-gtk3, and
restart the computer (or log out).
Alternatively, you can quit the ibus calling in a terminal 'ibus exit' before starting KiCad.
More details can be found in link:https://gitlab.com/kicad/code/kicad/-/issues/4547[GitLab Bug# 4547].


== Windows Specific Issues

=== Graphical Artifacts with Intel GPUs

Users with Intel integrated GPUs running outdated drivers may experience graphical artifacts
(usually horizontal lines) covering the editing canvases.  These artifacts are caused by bugs in
the graphics driver.  To solve this issue, update to the latest version of the Intel graphics
driver for your system.  Note that you may need to manually download and install an updated driver
on some systems; Windows Update does not always have a driver version new enough to resolve this
issue.

=== Network Shares

Editing projects located on Windows networks shares provided by samba servers (non-Windows Server,
i.e. Synology, FreeNAS) may result in crashes due to samba generating bad file change events and
our cross-platform toolkit asserting the invalid event rather than ignoring it. Windows Server
hosted shares should not encounter this issue.

You will have to work on a local drive if you encounter crashes working from a network drive for now.
More details can be found in link:https://gitlab.com/kicad/code/kicad/-/issues/5740[GitLab Bug #5740].


== macOS Specific Issues

=== 3Dconnexion Space Mouse

Support for the 3Dconnexion Space Mouse hardware is built-in to KiCad, but due to instability in
the drivers for macOS, this support is disabled and hidden behind an advanced configuration flag.

Users who wish to enable the support should add the following line to a file named
`~/Library/Preferences/kicad/7.0/kicad_advanced` (replacing `7.0` with a future KiCad version if
applicable):

`3DConnexionDriver=1`

Enabling this driver may cause KiCad to crash without notice.  Use this feature at your own risk,
and make sure to install the latest available version of the 3dconnexion drivers (which, at the
time of this writing, still contain some crash bugs).
