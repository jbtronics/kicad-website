+++
title = "KiCad 6.0.2 Release"
date = "2022-02-13"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.2 stable version contains critical bug fixes and
other minor improvements since the previous release.

<!--more-->

A list of all of the fixed bugs since the 6.0.1 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/11[KiCad 6.0.2
milestone page].  This release contains several critical bug fixes so
please consider upgrading as soon as possible.

Version 6.0.2 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

---

== Security Advisory

Multiple stack-based buffer overflow vulnerabilities were reported affecting KiCad 6.0.1 and earlier.
There is the possibility that those vulnerabilities can be used to execute malicious code when opening
gerber or excelon files from untrusted sources.

Users are strongly encouraged to update to KiCad 6.0.2 or later to be protected against the reported
vulnerabilities. KiCad Nightly users are strongly encouraged to update to a version released on
4. Feb 2022 or later. In case you are still running KiCad 5 or earlier, and your distribution is not
distributing a patched KiCad version, it is recommended to only open gerber and excelon files from trusted sources.

The KiCad team is not aware of any exploitation attempts towards those vulnerabilities. To trigger one
of the reported vulnerabilities, the user must open the malicious file in gerbview.

[%header,cols="1,1,2,4",role="table table-striped table-condensed"]
|===
| CVE                                                                       | CVSSv3                                                                                                           | Vulnerable Versions | Description
| https://cve.mitre.org/cgi-bin/cvename.cgi?name=2022-23946[CVE-2022-23946] | https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H&version=3.1[7.8] | \<= KiCad 6.0.1     | Stack-based buffer overflow in GCodeNumber parsing
| https://cve.mitre.org/cgi-bin/cvename.cgi?name=2022-23947[CVE-2022-23947] | https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H&version=3.1[7.8] | \<= KiCad 6.0.1     | Stack-based buffer overflow in DCodeNumber parsing
| https://cve.mitre.org/cgi-bin/cvename.cgi?name=2022-23803[CVE-2022-23803] | https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H&version=3.1[7.8] | \<= KiCad 6.0.1     | Stack-based buffer overflow in ReadXYCoord
| https://cve.mitre.org/cgi-bin/cvename.cgi?name=2022-23804[CVE-2022-23804] | https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H&version=3.1[7.8] | \<= KiCad 6.0.1     | Stack-based buffer overflow in ReadIJCoord
|===

=== Timeline

[%header,cols="1,4",role="table table-striped table-condensed"]
|===
| Date              | Event
| 1. February 2022  | CVE-2022-23946 and CVE-2022-23947 are reported by Cisco Talos
| 2. February 2022  | CVE-2022-23946 and CVE-2022-23947 are fixed in the master and 6.0 branch
| 2. February 2022  | CVE-2022-23803 and CVE-2022-23804 are reported by Cisco Talos
| 3. February 2022  | CVE-2022-23803 and CVE-2022-23804 are fixed in the master and 6.0 branch
| 13. February 2022 | KiCad 6.0.2 is released with all reported vulnerabilities corrected
|===

=== Technical Details

All reported vulnerabilities are stack-based buffer overflows located in file parsers for gerbview,
located in the functions `GCodeNumber`, `DCodeNumber`, `ReadXYCoord` and `ReadIJCoord`. A specially-crafted
gerber or excellon file can lead to code execution. An attacker can provide a malicious file that the
user has to load into gerbview to trigger this vulnerability. Contrary to reports in some vulnerability
databases, those vulnerabilities cannot be exploited remotely.

Stack-based buffer overflows allow attackers to control parts of the execution stack. In the simple case,
this results in an application crash. In some cases, attack techniques like return oriented programming (ROP)
or return to procedure linkage table (PLT) can be applied to execute custom code using the buffer overflow.
For now, no code execution exploit is known for the given vulnerabilities.

There are multiple limitations which will make achieving code execution hard in practice. As the vulnerability
is centered around loading a malicious file, a user has to download the given file and open it in gerbview.
For successful code execution, the buffer has to be rewritten to point to one or more specific locations in
the KiCad binary, which usually change with every build. Thus, the attacker has to know the exact build of KiCad
the user is using. Depending on the build settings of your distribution, additional attack mitigations are
applied to the binary which will make those vulnerabilities harder to attack.

An extensive writeup about the vulnerability was https://talosintelligence.com/vulnerability_reports/TALOS-2022-1460[published by Cisco Talos], who also reported the vulnerabilities.

=== Reporting a Security Vulnerability

The KiCad team takes security vulnerabilities seriously and will address them in a timely manner.To report
security issues, it is recommended to create a new issue in the https://gitlab.com/kicad/code/kicad/-/issues[KiCad bug tracker on Gitlab]
and set the checkbox _''This issue is confidential and should only be visible to team members with at least
Reporter access.''_ to ensure confidentiality.

The KiCad team asks security researchers to follow a coordinated vulnerability disclosure process to ensure
a corrected KiCad release is available to users before vulnerability details are published.

