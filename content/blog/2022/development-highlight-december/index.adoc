+++
title = "Development Highlight: December Edition, features coming to KiCad 7"
date = "2022-12-31"
draft = false
"blog/categories" = [
    "Development Highlight"
]
authors = [
  "mroszko"
]
+++

The end of 2022 is here and quite some work has been accomplished on various parts of KiCad over the year.

We are all working to hopefully have a KiCad 7 release by the end of January 2023 but no promises!

This is a highlight of some (but not all) of the new features added since the last development highlight.

<!--more-->

== General Improvements

=== PCM automatic updates
https://gitlab.com/qu1ck[qu1ck] in a follow up to the adding the PCM in 6.0 has now introduced automatic update checking of already installed Plugins and Content in
https://gitlab.com/kicad/code/kicad/-/merge_requests/1276[MR #1276]. 

Each time you launch KiCad, the plugin status is automatically queried in the background and displayed:

image::/img/blog/2022/pcm-update-notice.png[align=center, alt="Screenshot showing how PCM Updates are indicated"]

You can also disable the update checking behavior in Preferences.

image::/img/blog/2022/pcm-update-setting.png[align=center, alt="Screenshot showing how PCM Updates are indicated"]


=== Drag & Drop
lulu731 in https://gitlab.com/kicad/code/kicad/-/merge_requests/1244[MR #1244] added the drag and drop handles needed to various parts of KiCad
that results in the following actions:

* KiCad
** .kicad_pro, .pro files trigger opening project
** gerber and job files open Gerber Viewer
** Eagle and Cadstar files trigger open project
* Schematic Editor
** Schematic gets appended
* PCB Editor
** Append board
* Gerber Viewer
** Open files
* Drawing Sheet Editor
** Open sheet

=== macOS Apple Sillycon support
https://gitlab.com/adamwwolf[Adam Wolf] spent time in troubleshooting and identifying changes needed to get KiCad to build and package
correctly as native Apple silicon processors (M1, M2, etc) bundles. Resulting in https://gitlab.com/kicad/code/kicad/-/commit/98775afcbabddfe12ea40e5149b56cbd3496e0cc[commit 98775afc] and https://gitlab.com/kicad/packaging/kicad-mac-builder/-/merge_requests/376[MR #376 kicad-mac-builder] which took plenty of effort to decipher the correct process with Apple's continuing effort to locking down the macOS ecosystem for better or for worse.


=== Command Line Interface

https://gitlab.com/mroszko[Mark Roszko] in a slew of commits has added a new `kicad-cli` binary to the KiCad packages.

image::/img/blog/2022/cli.png[align=center, alt="Screenshot of cli invoked with -h"]

The separate `kicad-cli` binary was necessary to work around limitations on applications being both GUI and console on Windows at the executable level. 
Along with wxWidgets having a distinction between a GUI and console app for initializatin.

The `kicad-cli` brings all the necessary functions to automate export elements of schematic and pcb designs.

The currently avaiable commands are:
```
kicad-cli sch export bom
kicad-cli sch export netlist
kicad-cli sch export pdf
kicad-cli sch export svg
kicad-cli pcb export drill
kicad-cli pcb export dxf
kicad-cli pcb export gerber
kicad-cli pcb export pdf
kicad-cli pcb export pos
kicad-cli pcb export step
kicad-cli pcb export svg
```

You can use the `-h` argument to find out more about the command specific options.

These changes have also enabled the STEP export to work headless on Linux, unlike the original `kicad2step` binary.

We hope this is a useful mechanism for users to adopt.
We do plan to introduce a GUI based jobs system in KiCad 8 but see this as a interim solution.


== Schematic Editor Improvements


=== DNP fields
https://gitlab.com/sethhillbrand[Seth Hillbrand] in https://gitlab.com/kicad/code/kicad/-/commit/524b129c64969e251bcabf51994476a9358777f4[commit 524b129c]
added support for explicitly marking placed components in a schematic as "DNP" or Do Not Populate. The resulting components are also greyed out in the schematic
and excluded from any KiCad generated placement files.

image::/img/blog/2022/dnp-grey.png[Screenshot showing how PCM Updates are indicated, align=center]

image::/img/blog/2022/dnp-opt.png[align=center, alt="Screenshot showing how PCM Updates are indicated"]

=== Simulation Model Editor
https://gitlab.com/mwielgus[Mikolaj Wielgus] in https://gitlab.com/kicad/code/kicad/-/merge_requests/1220[MR #1220]
added a simulation model editor. This is a new dialog available as a "Simulation Model" button in the Symbol Properties of any schematic symbol.
It allows you to configure simulation model parameters in a GUI rather than spice text inserted on schematic and the resulting configuration gets passed to the simulator as expected. You can do everything from configuring a symbol as "standard" spice models (resistors,caps,diodes,bjts), to assigning custom
models in libraries (it will even load the library and let you select individual models contained within) and even let you customize the schematic pin
to spice pin pin mapping.

image::/img/blog/2022/simulation_model_editor.png[align=center, alt="Screenshot showing Simulation Model Editor dialog next to Symbol Properties Dialog"]

=== Database Libraries

A long requested feature by certain users of KiCad has been the ability to link component selection
to a database which is common in other schematic CAD tools. https://gitlab.com/craftyjon[Jon Evans] 
went ahead in https://gitlab.com/kicad/code/kicad/-/merge_requests/1282[MR #1282] to add connectivity
to external databases via ODBC functionality across KiCad's three main platforms.

The database library system allows creating a centralized database of symbol to footprint associations along with any metadata about your parts that can be shared across your schematics by you and others connected to the same database.

As a warning, KiCad's intention is only to utilize a database as a source of information. It will never provide an interface
to manage said information and it is up to user's to integrate it with some other external tooling as part databases get very 
workflow specific very fast.


More information can be found here: https://gitlab.com/kicad/code/kicad/-/issues/7436 and prelimary documentation is available here:
https://docs.kicad.org/master/en/eeschema/eeschema.html#database_libraries

An ongoing forum discussion about the feature is also here: https://forum.kicad.info/t/kicad-the-case-for-database-driven-design/34621


==== Dynamic field columns in symbol chooser

As part of the database libraries introduction, https://gitlab.com/craftyjon[Jon Evans] in https://gitlab.com/kicad/code/kicad/-/commit/e294fe2074b000f6b37f449f236dba04d7afd2aa[commit e294fe207] also introduced the ability to display custom fields in the symbol chooser.

image::/img/blog/2022/dynamic_columns.png[align=center, alt="Screenshot of the dynamic columns chooser in the select symbol dialog"]


=== Hyperlinks on Schematics

https://gitlab.com/jeffyoung[Jeff Young] and https://gitlab.com/Qbort[Roberto Fernandez Bautista]  in https://gitlab.com/kicad/code/kicad/-/commit/840bcffefb7f9108b72eeba715536c4320fe9bee[commit 840bcffefb7f9108b72eeba715536c4320fe9bee], https://gitlab.com/kicad/code/kicad/-/commit/9b007ca4bf042f9915faeef0f87e4aefcc40b4d3[commit 9b007ca4bf042f9915faeef0f87e4aefcc40b4d3] added support for hyperlinks inside schematics.
This means you can place a text object on a schematic page and fill in a new Hyperlink property that makes the text object become clickable.

Supported hyperlinks are:

- http://
- https://
- file://
- `#:<pagenumber>` (where <pagenumber> is any valid schematic page)

image::/img/blog/2022/sch_hyperlink.png[align=center, alt="Screenshot of the dynamic columns chooser in the select symbol dialog"]


=== PDF Improvements

==== PDF Bookmarks (Table of Contents)

https://gitlab.com/mroszko[Mark Roszko] in https://gitlab.com/kicad/code/kicad/-/commit/6f8205235fabf5a7712af0e06a214b93a7afbdf9[commit 6f8205235] added support 
for populating the Bookmarks section (also known as the Table of Contents) of a PDF viewer with useful information.

Now in a schematic PDF generated by the Plot action in the Schematic Editor, you will find bookmarks populated with a list of sheets and links to symbols within
the sheet. Clicking on items on the bookmark list will jump to the relevant page and location.

image::/img/blog/2022/pdf-bm.png[align=center, alt="Screenshot of Adobe Reader showing Bookmarks in a plotted schematic"]


==== Component Info
https://gitlab.com/jeffyoung[Jeff Young] in https://gitlab.com/kicad/code/kicad/-/commit/8afc1db7a61298413336b591b1ab22788c6cf04e[commit 8afc1db7a6] added the export of placed component info in schematics to the plotted PDF. This is a list of component field data displayed
when the symbol is clicked in a PDF viewer. Unforunately, the functionality required for this to work is implemented per PDF spec but support across PDF viewers is not consistent. Adobe PDF Reader/Acrobat are known to display the list correctly.


image::/img/blog/2022/pdf-compinfo.png[align=center, alt="Screenshot showing the component info added to plotted PDFs"]

==== PDF Hyperlinks

Also added by https://gitlab.com/jeffyoung[Jeff Young] in addition to Component Info and the implementation of schematic hyperlinks by Roberto, hyperlinks are also embedded into the PDF
and interactable.

This includes:

- Hyperlinks between pages
- Hyperlinks to external URLs
- Sheet symbols link to datasheets


== PCB Editor Improvements

=== Various PCB Layout Tool Enhancements
https://gitlab.com/mikebwilliams[Mike Williams] has worked on various quality of life additions 

==== Background bitmaps in the board and footprint editor
A sometimes requested feature by some users has been the ability to display an image in the background 
so that PCB traces can be copied easily from some original reference PCB being reverse engineered.

This is now possible since https://gitlab.com/kicad/code/kicad/-/commit/3669cb46739cb5af2c73d7801c1c18e7a514bbee[commit 3669cb46] and improved in https://gitlab.com/kicad/code/kicad/-/commit/99f8b21ed142fb837d3060148cd6fa6008017ae3[commit 99f8b21ed]
to allow configuring a bitmap per board layer, making it easier for reverse engineering multi-layer boards.

++++
<div class="text-center ratio ratio-16x9">
    <video controls muted playsinline class="embed-responsive-item">
        <source src="/video_clips/blog/2022/pcb_bitmaps.mp4" type="video/mp4">
    </video>
</div>
++++

==== Unroute Selected
Ever wanted to just completely unroute a footprint for reasons?
https://gitlab.com/kicad/code/kicad/-/commit/ca0c9f12e0ccb7668894490fee14e3158813b023[commit ca0c9f12e0] has made this possible.

++++
<div class="text-center ratio ratio-16x9">
    <video controls muted playsinline class="embed-responsive-item">
        <source src="/video_clips/blog/2022/unroute-selected.mp4" type="video/mp4">
    </video>
</div>
++++

==== Auto-complete trace route
A nifty new utility added in https://gitlab.com/kicad/code/kicad/-/commit/1a023f5dd0932fba43ad109c22db8ccc75a127d6[commit 1a023f5dd] 
that may be invaluable to power users with Hot keys, you can now draw traces partially
and ask the PCB Editor to complete the trace the rest of the way to the nearest ratsnest end. 
It will stop and force you to manually route if it can't find a route.

++++
<div class="text-center ratio ratio-16x9">
    <video controls muted playsinline class="embed-responsive-item">
        <source src="/video_clips/blog/2022/finish-routing.mp4" type="video/mp4">
    </video>
</div>
++++

==== Attempt to Finish Selection
Related to the trace route completion feature, you can now select items and ask the PCB Editor to attempt to "auto route" traces between them.
It will iterate each unrouted part of the item and attempt to connect it to the nearest rats nest.

WARNING: This is not intended to be an autorouter but instead a useful power tool to make connections quickly that sort of acts like one 😉

++++
<div class="text-center ratio ratio-16x9">
    <video controls muted playsinline class="embed-responsive-item">
        <source src="/video_clips/blog/2022/fauxauto1.mp4" type="video/mp4">
    </video>
</div>
++++

++++
<div class="text-center ratio ratio-16x9">
    <video controls muted playsinline class="embed-responsive-item">
        <source src="/video_clips/blog/2022/fauxauto2.mp4" type="video/mp4">
    </video>
</div>
++++

=== Search Panel
https://gitlab.com/mroszko[Mark Roszko] added in https://gitlab.com/kicad/code/kicad/-/commit/f304e2d4f63c1047d39440aa2f7608a282698ae3[commit f304e2d4f] a new search panel

This panel in the PCB Editor enables typing query strings with wildcards and filtering out objects on the design while displaying all of the results in a list. The list can be clicked on
to select objects on the editing canvas.

image::/img/blog/2022/search_pane.png[align=center, alt="Screenshot showing the new search pane"]

=== Properties Panel
https://gitlab.com/craftyjon[Jon Evans] took over work that was years ago started by Maciej Suminski to implement a unified properties system for editing within KiCad.
The purpose of a unified properties system is to enable dynamically generated GUI interfaces that can be reused across multiple types of items rather than having to
create dialogs for every possible object and it's editing options.

The Properties Panel is now available in the PCB Editor as a result.

image::/img/blog/2022/properties.png[align=center, alt="Screenshot showing properties panel with properties for a text item"]

One immediate benefit is the Properties Panel allows multi-editing objects with similar properties, simply select the objects you want together and the potential options are displayed.

Future plans beyond 7.0 will be bringing the properties panel to the Schematic Editor and Drawing Sheets Editor.


=== Improved footprint spread and Pack & Move Footprint Tool

Have you ever drawn a large schematic and then finally opened the PCB board to begin a layout and find yourself staring at a 
jumbled mess of components to place? Well fear not!

https://gitlab.com/dsa-t[dsa-t] in https://gitlab.com/kicad/code/kicad/-/merge_requests/1325[MR #1325]
introduced some improvements to the initial setup of a PCB layout.

First the algorithim that placed components on the PCB was improved to result in a more compact result.

Here is a before and after example of this change:

image::/img/blog/2022/pm-before.png[align=center, width=50%, alt="Screenshot showing a sample initial component placement under the old algorithim"]
image::/img/blog/2022/pm-after.png[align=center, width=50%, alt="Screenshot showing a sample initial component placement under the new algorithim"]


Secondly, a powerful new tool was added that let's you select a logical portion of your schematic yourself in the Schematic Editor
and with a new right click menu option in the PCB Editor, you can now "Pack and Move" the selected components as one compacted group
rather than having to jump around all over the PCB Editor to locate and drag components.

++++
<div class="text-center ratio ratio-16x9">
    <video controls muted playsinline class="embed-responsive-item">
        <source src="/video_clips/blog/2022/pack-footprints.mp4" type="video/mp4">
    </video>
</div>
++++

=== STEP Exporter Changes

https://gitlab.com/mroszko[Mark Roszko] in a series of commits overhauled the STEP exporter to use the same PCB parsing engine as the rest of KiCad. 
It's independence from the main KiCad codebase resulted in various bugs over time and became hard to maintain as the main PCB format evolved.

This change now brings the addition relative 3d model paths to the footprint. Simply start the paths with the expected ./ or ../ and the 
3D viewer and STEP exporter will attempt to find the model relative to the footprint file. This change should allow better portability
of footprint libraries.

Finally, "3D Path Aliases" were removed from KiCad. These were an independent system from the "Environment Variable" system also used for
paths. It had it's own file format and usage syntax which both introduced unexpected bugs. Unifying the system makes the usage of Environment Variables
standard throughout KiCad. "3D Path Aliases" will be automigrated to an Environment variable on launch. However, the alias may get renamed
if the alias had characters beyond uppercase A-Z,0-9 and underscores due to the limitations of Environment Variables across platforms.

== End of Year Wrap Recap

The KiCad developers held a recap livestream on December 17th, 2022. It covers a few more features and discussions on the future of KiCad.

+++
<div class="ratio ratio-16x9">
    <iframe src="https://www.youtube.com/embed/iQrdXf7SLkw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
+++


== 2022 Codebase Contributors
The above features weren't the only contributions made to KiCad, we would like to thank all of the contributors this year for contributing to the KiCad codebase (this includes translators in this list!)

  1481  Jeff Young
   576  jean-pierre charras
   510  Seth Hillbrand
   372  Mark Roszko
   205  Mikolaj Wielgus
   202  Jon Evans
   157  Mike Williams
   109  Alex
    95  Wayne Stambaugh
    77  Henrik Kauhanen
    70  Tomasz Wlostowski
    67  Roberto Fernandez Bautista
    59  Ivan Chuba
    49  co8 j
    48  Ulices
    46  taotieren
    45  Eric
    43  ZbeeGin
    41  Wellington Terumi Uemura
    40  Mark Hämmerling
    37  Jan Straka
    37  Toni Laiho
    33  Ian McInerney
    32  Arnau Llovet Vidal
    30  Thomas Pointhuber
    25  Fabien Corona
    25  김랑기
    23  markus-bonk
    20  Sylwester Kocjan
    20  qu1ck
    19  Tokita, Hiroshi
    19  gurqja3810
    16  Marco Ciampa
    16  WhiteChairFromIkea
    16  aris-kimi
    15  dsa-t
    14  Mustafa Selçuk ÇAVDAR
    12  Maciej Suminski
    10  Simon Richter
    10  이기형
     9  Jonathan Haas
     9  boonchai k
     8  Ola Rinta-Koski
     8  YunJiSang
     7  Steffen Mauch
     7  VicSanRoPe
     7  박세훈
     6  Axel Henriksson
     6  Kevin Lannen
     6  Kuzemko Alexsandr
     6  Miklós Márton
     6  kmn4555
     5  KwonHyeokbeom
     5  Simon Schaak
     5  Tian Yunhao
     4  Ettore Atalan
     4  Graham Keeth
     4  Jan Mrázek
     4  Konstantin Baranovskiy
     4  Mario Luzeiro
     4  Neko Nekowazarashi
     4  ky107601
     4  obit
     4  김세영
     3  Andrey Fedorushkov
     3  Bevan Weiss
     3  Jakub Janek
     3  Rigo Ligo
     3  kliment
     3  minsu kim (0xGabriel)
     2  Adam Wolf
     2  Alex Gellen
     2  Alexander Dewing
     2  Allan Nordhøy
     2  Barabas Raffai
     2  Bas Wijnen
     2  Brian Fiete
     2  Eduardo Behr
     2  Frank Zeeman
     2  Greg Davill
     2  Grzegorz Szymaszek
     2  Huanyin Liu
     2  Kliment
     2  Krzysztof Kawa
     2  Miklos Marton
     2  Rafael Silva
     2  Scott Hanson
     2  Steven A. Falco
     2  ___davidpr
     2  lulu731
     2  luz paz
     2  lê văn lập
     2  prometheus
     2  김용재
     2  이윤성
     1  Adam Wahab
     1  Andrii Shelestov
     1  Artem
     1  Benedikt Freisen
     1  Bobbe
     1  Brian Mayton
     1  BugRepellentExtraStrong
     1  Bế Trọng Nghĩa
     1  Chetan Shinde
     1  CloverGit
     1  Daniil Nikolaev
     1  Davide Gerhard
     1  Dejan Smole
     1  Evils
     1  Forrest Voight
     1  Franck
     1  Franck Bourdonnec
     1  Francois Berder
     1  Frank Palazzolo
     1  Gökhan Koçmarlı
     1  Hanna Breisand
     1  Håvard Syslak
     1  J RB
     1  Johannes Maibaum
     1  Jose Perez
     1  Keisuke Nakao
     1  Lorenzo Marcantonio
     1  Mads Dyrmann
     1  Marcel Hecko
     1  Marco Langer
     1  Marcus A. Romer
     1  Michael Misirlis
     1  Michal Sojka
     1  Ondřej Čertík
     1  Pavel Dovgalyuk
     1  Peter B
     1  Petri Niemelä
     1  Petter Reinholdtsen
     1  Radek K
     1  Sašo Domadenik
     1  Scott Candey
     1  Stanislav Kaliuk
     1  Stefan
     1  Stefan Hamminga
     1  Tanay Gupta
     1  Tom Keddie
     1  Trần Phi Hải
     1  Vesa Solonen
     1  Vitan Košpenda
     1  Werni
     1  Yang sheng
     1  Zoltan Puskas
     1  Zoran
     1  alexfanqi
     1  canwail
     1  dana
     1  frutiemax
     1  hogthrob
     1  jeongsuAn
     1  leonardokr
     1  lukas-heiligenbrunner
     1  magical-obama
     1  maksz42
     1  passionateengineer
     1  pondahai
     1  ssantos
     1  willliam
     1  yangyangdaji
     1  МАН69К
     1  木 王
     1  김낙환
     1  김호진
     1  남우근
     1  이상수